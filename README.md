# Unity-Build-Manager-QT
A QT Port of the Unity Build Manager

Windows Support - Pending

Mac Support - Peding

Linux Support - Pending

###### NOTE: This application is still in progress and might not fully function.

# Installation

1. Download and Execute installer(.exe for Windows, .dmg for Mac)

2. Install the program to a location you specify

3. Windows: Launch the application from the shortcut/location specified for installation; Mac: Launch the application from the applications list/location specified for installation

# How to use

1. Start by specifying a name for your build in the 'Build Name' text field

2. Add a Build by pressing the 'Add' button. Click the browse button beside the 'Project Location' text field and browse to the folder holding your unity project. Click the Target drop down to select your Architecture and OS target. Then specify a build directory for the packaged version of your project to be outputted to. Click 'OK' to confirm your build settings.

3. Click 'Save' to save a .UBM file with your build settings

4. (Optional) Click 'Set Current as Default' to set the currently loaded .UBM to always load when the program starts

5. When you are ready, click 'Build' to build all the builds currently in the build list

This application is licensed under the GPL General Public License v2. View the file called "LICENSE" for details.
